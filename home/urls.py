from django.urls import path
from . import views

urlpatterns = [
    path('', views.webpage1, name='webpage1'),
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutUser, name='logout'),
    path('register/', views.registerPage, name='register'),
]