from django.test import TestCase, Client, RequestFactory
from django.apps import apps
from django.urls import reverse, resolve
from . import views
from . import models
from home.apps import HomeConfig
from .views import loginPage

class Tests(TestCase):
    def test_list_url_is_resolved_pageselamatdatang(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(HomeConfig.name, 'home')
        self.assertEqual(apps.get_app_config('home').name, 'home')
    
    def test_logout(self):
        self.client = Client()
        self.client.login(username='ravi', password='ravi')
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 302)

    

    def test_loginPage_else(self):
        test = 'Anonymous'
        response_post = Client().post('/login/', {'username': test, 'password': test})
        self.assertEqual(response_post.status_code, 200)

    def test_registerPage_else(self):
        test = 'Anonymous'
        response_post = Client().post('/register/', {'username': test, 'password': test})
        self.assertEqual(response_post.status_code, 200)




    